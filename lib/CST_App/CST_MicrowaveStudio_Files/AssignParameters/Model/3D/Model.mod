'# MWS Version: Version 2021.5 - Jun 28 2021 - ACIS 30.0.1 -

'# length = mm
'# frequency = GHz
'# time = s
'# frequency range: fmin = 1.000000 fmax = 10.000000
'# created = '[VERSION]2021.5|30.0.1|20210628[/VERSION]


'@ Set Units

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Units
.Geometry "mm"
.Frequency "GHz"
.Time "S"
.TemperatureUnit "Kelvin"
.Voltage "V"
.Current "A"
.Resistance "Ohm"
.Conductance "Siemens"
.Capacitance "PikoF"
.Inductance "NanoH"
End With

'@ SetFrequency

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
Solver.FrequencyRange "1.000000", "10.000000"

'@ define boundaries

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Boundary
.Xmin "expanded open"
.Xmax "expanded open"
.Ymin "expanded open"
.Ymax "expanded open"
.Zmin "expanded open"
.Zmax "expanded open"
End With

'@ Set Background Material

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Material
.Type "Normal
.Colour "0.6", "0.6", "0.6"
.Epsilon "1"
.Mu "1"
.ChangeBackgroundMaterial
End With

'@ define material: Material_1

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Material
.Reset
.Name "Material_1"
.Type "Normal"
.Epsilon "2.2"
.Mue "1"
.TanD "0"
.TanDFreq "0.0"
.TanDGiven "False"
.TanDModel "ConstTanD"
.Sigma "0"
.TanDM "0"
.TanDMFreq "0.0"
.TanDMGiven "False"
.TanDMModel "ConstTanD"
.SigmaM "0"
.Colour "1.000000", "0.000000", "0.000000"
.Create
End With

'@ define material: Material_2

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Material
.Reset
.Name "Material_2"
.Type "Normal"
.Epsilon "Er"
.Mue "Mue"
.TanD "0"
.TanDFreq "0.0"
.TanDGiven "False"
.TanDModel "ConstTanD"
.Sigma "0"
.TanDM "0"
.TanDMFreq "0.0"
.TanDMGiven "False"
.TanDMModel "ConstTanD"
.SigmaM "0"
.Colour "0.000000", "0.000000", "1.000000"
.Create
End With

'@ define brick: Component1:Brick1

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Brick
.Reset
.Name "Brick1"
.Component "Component1"
.Material "Material_1"
.XRange "-5", "5"
.YRange "-5", "5"
.ZRange "-5", "5"
.Create
End With

'@ define brick: Component1:Brick2

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Brick
.Reset
.Name "Brick2"
.Component "Component1"
.Material "Material_2"
.XRange "6", "16"
.YRange "-5", "5"
.ZRange "-5", "5"
.Create
End With

'@ delete component: Component1

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
component.Delete "Component1"

'@ define brick: Component1:Brick1

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Brick
.Reset
.Name "Brick1"
.Component "Component1"
.Material "Material_1"
.XRange "x1", "x2"
.YRange "y1", "5"
.ZRange "-5", "5"
.Create
End With

'@ define brick: Component1:Brick2

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Brick
.Reset
.Name "Brick2"
.Component "Component1"
.Material "Material_2"
.XRange "6", "16"
.YRange "-5", "5"
.ZRange "-5", "z2"
.Create
End With

'@ transform:Component1:Brick1

'[VERSION]2021.5|30.0.1|20210628[/VERSION]
With Transform
.Reset 
.Name "Component1:Brick1"
.Vector "xshift", "0", "1"
.UsePickedPoints "False"
.InvertPickedPoints "False"
.MultipleObjects "False"
.GroupObjects "False"
.Repetitions "1"
.MultipleSelection "False"
.Destination "Component1"
.Material ""
.Transform "Shape", "Translate"
End With

