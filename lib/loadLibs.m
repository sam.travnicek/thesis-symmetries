function loadLibs()
    [dirname] = fileparts(mfilename('fullpath'));
    addpath(fullfile(dirname, 'Custom'));
    addpath(fullfile(dirname, 'CST_App'));
    addpath(fullfile(dirname, 'TCSTInterface', 'Functions'));
end