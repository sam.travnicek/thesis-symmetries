classdef MonopoleArrayGenerator < handle

    properties (Access = private)
        cst = []
        mainMonopoleLen = 1
        gap = 0.1
        groundPadding = 10
        groundThickness = 1
        monopoleRadius = 1
        material = 'PEC'
        componentName = 'component1'
        feedCurrent = 1.0
        feedImpedance = 50.0
        portType = 'SParam'
        rings = []
        tmp_lastLoadNumber = 1
    end

    methods(Static)

        function name = createElementName(name, radius, angle)
            radius = round(radius*100)/100;
            angle = round(angle*100)/100;
            name = strcat([...
                name ' - ' num2str(radius) ' - ' num2str(angle)]);
        end

        function name = createMonopoleName(radius, angle)
            name = MonopoleArrayGenerator.createElementName(...
                'Monopole', radius, angle);
        end

        function name = createLoadName(radius, angle)
            name = MonopoleArrayGenerator.createElementName(...
                'Load', radius, angle);
        end

    end

    methods (Access = private)

        function buildProjectWithPorts(obj)
            obj.tmp_lastLoadNumber = 1;
            obj.buildBase();
            obj.buildRingsWithPorts();
        end

        function buildProjectWithLoads(obj)
            obj.tmp_lastLoadNumber = 1;
            obj.buildBase();
            obj.buildRingsWithLoads();
        end

        function buildBase(obj)
            obj.buildGround();
            obj.buildMainMonopole();
            obj.buildMainPort();
        end

        function radius = getMaxRadius(obj)
            radius = 0;
            for i=1:length(obj.rings)
                radius = max(radius, obj.rings(i).radius);
            end
        end

        function radius = getGroundRadius(obj)
            radius = obj.getMaxRadius() + obj.groundPadding;
        end

        function buildGround(obj)
            outerRadius = obj.getGroundRadius();
            obj.cst.addCylinder(...
                outerRadius, 0, 'z', 0, 0, [-obj.groundThickness 0], ...
                'Ground Plane', obj.componentName, obj.material)
        end

        function buildMonopole(obj, radius, angle, length)
            z1 = obj.gap;
            z2 = z1 + length;
            posX = radius*cos(angle);
            posY = radius*sin(angle);
            name = MonopoleArrayGenerator.createMonopoleName(radius,angle);
            obj.cst.addCylinder(...
                obj.monopoleRadius, 0, 'z', posX, posY, [z1 z2], ...
                name, obj.componentName, obj.material)
        end

        function buildMainMonopole(obj)
            obj.buildMonopole(0, 0, obj.mainMonopoleLen);
        end

        function buildMainPort(obj)
            obj.buildPort(0, 0);
        end

        function buildPort(obj, radius, angle)
            posX = radius*cos(angle);
            posY = radius*sin(angle);
            if strcmp(obj.portType, 'SParam')
                obj.cst.addDiscretePort(...
                    [posX posX], [posY posY], [0 obj.gap], 0.1, ...
                    obj.feedImpedance);
            elseif strcmp(obj.portType, 'Current')
                obj.cst.addCurrentDiscretePort(...
                    [posX posX], [posY posY], [0 obj.gap], 0.1, ...
                    obj.feedCurrent);
            end
        end

        function buildRingsWithPorts(obj)
            for i=1:length(obj.rings)
                obj.buildRingWithPorts(obj.rings(i));
            end
        end

        function buildRingsWithLoads(obj)
            for i=1:length(obj.rings)
                obj.buildRingWithLoads(obj.rings(i));
            end
        end

        function buildRingWithPorts(obj, ring)
            numMonopoles = size(ring.RLCs, 1);
            for n=1:numMonopoles
                angle = 2*pi/numMonopoles*(n-1)+ring.rotation;
                obj.buildMonopole(ring.radius, angle, ring.length);
                obj.buildPort(ring.radius, angle);
            end
        end

        function buildRingWithLoads(obj, ring)
            numMonopoles = size(ring.RLCs, 1);
            for n=1:numMonopoles
                angle = 2*pi/numMonopoles*(n-1)+ring.rotation;
                obj.buildMonopole(ring.radius, angle, ring.length);
                obj.buildLoad(ring.radius, angle, ring.RLCs(n,:));
            end
        end

        function buildLoad(obj, radius, angle, RLC)
            obj.tmp_lastLoadNumber = obj.tmp_lastLoadNumber + 1;
            posX = radius*cos(angle);
            posY = radius*sin(angle);
%             name = MonopoleArrayGenerator.createLoadName(radius, angle);
            name = ['Load ' num2str(obj.tmp_lastLoadNumber)];
            obj.cst.addRLCLumpedElement(...
                name,[posX posX],[posY posY],[0 obj.gap], ...
                RLC(1), RLC(2), RLC(3), 'Serial')
        end

    end

    methods

        function setInterface(obj, newValue)
            obj.cst = newValue;
        end

        function setMainMonopoleLength(obj, newValue)
            obj.mainMonopoleLen = newValue;
        end

        function setGap(obj, newValue)
            obj.gap = newValue;
        end

        function setGroundPadding(obj, newValue)
            obj.groundPadding = newValue;
        end

        function setGroundThickness(obj, newValue)
            obj.groundThickness = newValue;
        end

        function setMonopoleRadius(obj, newValue)
            obj.monopoleRadius = newValue;
        end

        function setMaterial(obj, newValue)
            obj.material = newValue;
        end

        function setComponentName(obj, newValue)
            obj.componentName = newValue;
        end

        function setFeedCurrent(obj, newValue)
            obj.feedCurrent = newValue;
        end

        function setFeedImpedance(obj, newValue)
            obj.feedImpedance = newValue;
        end

        function setPortTypeToSParam(obj)
            obj.portType = 'SParam';
        end

        function setPortTypeToCurrent(obj)
            obj.portType = 'Current';
        end

        function setPortType(obj, newValue)
            obj.portType = newValue;
        end

        function addRing(obj, position, length, RLCs)
            % position = [radius rotationInDegrees]
            % RLCs - matrix with RLC values for every element:
            % R1 L1 C1
            % R2 L2 C2
            %   ...
            obj.rings = [obj.rings; struct(...
                'radius', position(1), 'rotation', position(2), ...
                'length', length, 'RLCs', RLCs)];
        end

        function generateWithPorts(obj)
            obj.buildProjectWithPorts();
        end

        function generateWithLoads(obj)
            obj.buildProjectWithLoads();
        end

    end

end