function integralTotal=getInnerProductFFComp(ff1, ff2)
    % ff1, ff2 ... farfield(nTheta,nPhi)
    
    if size(ff1,1) ~= size(ff2,1) || size(ff1,2) ~= size(ff2, 2)
        error('Farfields size mismatch!');
    end
    
    nTheta = size(ff1,1);
    nPhi = size(ff1,2);
    dTheta = pi/(nTheta-1);
    dPhi = pi/(nPhi-1);
    
    thetaVec = sin((0:nTheta-1)*dTheta);
    thetaGrid = repmat(thetaVec', 1, nPhi);
    dS = 2*thetaGrid*sin(dTheta/2)*dPhi;
%     dS = sin(thetaGrid)*dTheta*dPhi;
    
    
    integrandTotal = conj(ff1(1:end-1,1:end-1)).*ff2(1:end-1,1:end-1).*dS(1:end-1,1:end-1);
    integralTotal = sum(sum(integrandTotal));             
end