function plotRadiationPatternZUp(f, theta, phi)
    theta = theta/pi*180;
    phi = phi/pi*180;
    patternCustom(f',theta,phi);
    axesHand = gca;
    axis(axesHand,'normal')
    axesHand.DataAspectRatio = [1 1 1];
    set(axesHand, 'Projection', 'perspective');
    axis vis3d; 
    zoom(1.7);
%     rotate3d on;
    set(axesHand, 'Clipping', 'on');
    set(axesHand, 'ClippingStyle', 'rectangle');
%     camlight;
    c = camlight('headlight');    % Create light
    set(c,'style','infinite');    % Set style
    h = rotate3d(axesHand);                 % Create rotate3d-handle
    h.ActionPostCallback = @RotationCallback; % assign callback-function
    h.Enable = 'on';              % no need to click the UI-button
    h.RotateStyle = 'orbit';
    material dull;
%     hPlot.AmbientStrength = 0.2;

    % Sub function for callback
    function RotationCallback(~,~)
        c = camlight(c,'headlight');
    end
end