function plotField(field, min_dB, iPort, iFreq)
    if strcmp(field.VectorComponents, 'theta-phi') == 0
        error('Expected ''theta-phi'' vector components.');
    end
    E_components = field.E(:,:,:,iPort,iFreq);
    f = 10*log10(squeeze(sum(abs(E_components).^2, 3)));
    f = f - max(f);
    f(f < min_dB) = min_dB;
    patternCustom(f', field.THETA(:,1)/pi*180, field.PHI(1,:)/pi*180);
    zoom(2);
    rotate3d on;
    camup([0 1 0]);
end