function E_components = getEComponents(field, iPort, iFreq)
    % E_components(thetaRad,phiRad,:) = E_rho(=0), E_theta, E_phi
    % Units: V/m
    if strcmp(field.VectorComponents, 'theta-phi') == 0
        error('Expected ''theta-phi'' vector components.');
    end
    E_components = squeeze(field.E(:,:,:,iPort,iFreq));
end