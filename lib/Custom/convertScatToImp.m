function Zparams = convertScatToImp(Sparams, Zref)
    Zparams = complex(zeros(size(Sparams)));
    E = eye(size(Sparams, 1));
    sqrtZ = E*sqrt(Zref(:));
    for n=1:size(Sparams,3)
        Sn = Sparams(:,:,n);
        Zparams(:,:,n) = sqrtZ*((E-Sn)\(E+Sn))*sqrtZ;
    end
end