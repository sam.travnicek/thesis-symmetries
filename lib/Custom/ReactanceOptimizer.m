classdef ReactanceOptimizer < handle
    % optimization criterion:
    %   'full' ... optimizes magnitudes and angles of E_phi and E_theta each separately
    %   'mag' ... optimizes magnitudes of E_phi and E_theta each separately
    %   'sum' ... optimizes sqrt(abs(E_theta).^2 + abs(E_phi).^2)
    
    properties (Access = private)
        portFF = {}
        coords = []
        Zmat = []
        Ibounds = [1e-3 1e-1]
        Xbounds = [-150 150]
        resultVars = []
        refFF = []
        optimizerOutput = []
        minCost = Inf
        computeCrit = @(ffTst, ffRef) ReactanceOptimizer.computeCritSum(ffTst, ffRef);
    end

    methods(Static, Access = private)

        function cost = computeCritFull(ffTst, ffRef)
            ffDif = ffRef - ffTst;
            ipDif = getInnerProductFF(ffDif, ffDif);
            ipRef = getInnerProductFF(ffRef, ffRef);
            cost = ipDif/ipRef;
        end

        function cost = computeCritMag(ffTst, ffRef)
            ffDif = abs(ffRef) - abs(ffTst);
            ipDif = getInnerProductFF(ffDif, ffDif);
            ipRef = getInnerProductFF(ffRef, ffRef);
            cost = ipDif/ipRef;
        end

        function cost = computeCritSum(ffTst, ffRef)
            ffDif = sqrt(sum(abs(ffRef(:,:,1)).^2, 3)) - sqrt(sum(abs(ffTst(:,:,1)).^2, 3));
            ipDif = getInnerProductFFComp(ffDif, ffDif);
            ipRef = getInnerProductFF(ffRef, ffRef);
            cost = ipDif/ipRef;
        end

   end
    
    methods (Access = private)
        
        function cost = computeCost(obj, vars)
            ff = obj.generateFF(vars(1), vars(2:end));
            cost = obj.computeCrit(ff, obj.refFF);
            obj.minCost = min(obj.minCost, cost);
        end
        
    end
    
    methods
        
        function setPortFarfields(obj, portFF)
            obj.portFF = portFF;
        end
        
        function setCoordinates(obj, coords)
            obj.coords = coords;
        end
        
        function setImpedanceMatrix(obj, Zmat)
            obj.Zmat = Zmat;
        end
        
        function setExcitationCurrentBounds(obj, bounds)
            obj.Ibounds = bounds;
        end
        
        function setReactanceBounds(obj, bounds)
            obj.Xbounds = bounds;
        end
        
        function setReferenceFF(obj, ff)
            obj.refFF = ff;
        end

        function setOptimizationCriterion(obj, crit)
            if strcmp(crit, 'full')
                obj.computeCrit = @(ffTst, ffRef) ReactanceOptimizer.computeCritFull(ffTst, ffRef);
            elseif strcmp(crit, 'mag')
                obj.computeCrit = @(ffTst, ffRef) ReactanceOptimizer.computeCritMag(ffTst, ffRef);
            elseif strcmp(crit, 'sum')
                obj.computeCrit = @(ffTst, ffRef) ReactanceOptimizer.computeCritSum(ffTst, ffRef);
            else
                error(strcat("Unknown optimization criterion: '", crit, "'!"));
            end
        end
        
        function run(obj)
            obj.minCost = Inf;
            numPorts = length(obj.portFF);
            nvars = numPorts; %(1 current + (numPorts-1) reactances)
            bounds = [obj.Ibounds; repmat(obj.Xbounds, nvars-1, 1)];
            [vars,~,~,output] = particleswarm(...
                @(vars) obj.computeCost(vars), nvars, bounds(:,1), bounds(:,2));
            obj.resultVars = vars;
            obj.optimizerOutput = output;
        end
        
        function ff = getResultFF(obj)
            ff = obj.generateFF(obj.getResultExcitationCurrent(), obj.getResultReactances());
        end
        
        function I = getResultExcitationCurrent(obj)
            I = obj.resultVars(1);
        end
        
        function X = getResultReactances(obj)
            X = obj.resultVars(2:end);
        end
        
        function output = getOptimizerOutput(obj)
            output = obj.optimizerOutput;
        end

        function cost = getMinimumCost(obj)
            cost = obj.minCost;
        end
        
        function ffTot = generateFF(obj, Iexcit, x)
            % Generate farfield from excitation current and reactance vector
            Z_EE = obj.Zmat(1,1);
            Z_LE = obj.Zmat(2:end,1);
            Z_LL = obj.Zmat(2:end,2:end);
            Zi = 1i*x;
            Z_L = diag(Zi);
            Z_a = (-Z_L-Z_LL)^-1 * Z_LE;
            Z_w = [eye(size(Z_EE,1)); Z_a];
            I_E = [ Iexcit ];
            I = Z_w*I_E;
            ffTot = 0;
            for i=1:length(obj.portFF)
                ffTot = ffTot + obj.portFF{i}*I(i);
            end
        end
        
    end
    
end