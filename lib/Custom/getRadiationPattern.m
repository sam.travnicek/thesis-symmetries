function f = getRadiationPattern(field, iPort, iFreq)
    if strcmp(field.VectorComponents, 'theta-phi') == 0
        error('Expected ''theta-phi'' vector components.');
    end
    E_components = field.E(:,:,:,iPort,iFreq);
    f = squeeze(sqrt(sum(abs(E_components).^2, 3)));
end