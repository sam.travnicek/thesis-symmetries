function [index, distance] = findClosestIndex(desiredValue, values)
    [distance, index] = min(abs(desiredValue - values));
end
