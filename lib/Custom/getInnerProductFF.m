function [innerProductTotal,innerProductTheta,innerProductPhi]=getInnerProductFF(ff1, ff2)
    % ff1, ff2 ... farfield(nTheta,nPhi,2)
    %   third dimension contains E components: E_theta, E_phi
    
    if size(ff1,1) ~= size(ff2,1) || size(ff1,2) ~= size(ff2, 2)
        error('Farfields size mismatch!');
    end
    
    nTheta = size(ff1,1);
    nPhi = size(ff1,2);
    dTheta = pi/(nTheta-1);
    dPhi = pi/(nPhi-1);
    
    thetaVec = sin((0:nTheta-1)*dTheta);
    thetaGrid = repmat(thetaVec', 1, nPhi);
    dS = 2*thetaGrid*sin(dTheta/2)*dPhi;
%     dS = sin(thetaGrid)*dTheta*dPhi;
    
    integrandTheta = conj(ff1(1:end-1,1:end-1,1)).*ff2(1:end-1,1:end-1,1).*dS(1:end-1,1:end-1);
    integrandPhi = conj(ff1(1:end-1,1:end-1,2)).*ff2(1:end-1,1:end-1,2).*dS(1:end-1,1:end-1);
    
    integralTheta = sum(sum(integrandTheta));
    integralPhi = sum(sum(integrandPhi));

    innerProductTheta = integralTheta;
    innerProductPhi = integralPhi;
    innerProductTotal = integralTheta+integralPhi;                
end