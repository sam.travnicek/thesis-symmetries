close all;
clear;
clc;

addpath('../lib');
loadLibs();

global F0 MIN_F MAX_F;
global LIGHT_SPEED;
global RINGS;
global MONOPOLE_RADIUS;
global MAIN_MONOPOLE_LENGTH;
global GND_PLANE_PADDING;
global GND_THICKNESS;
global GAP;
global CNUM_NEAR CNUM_FAR;

%==================================================================================================%
% Constants:                                                                                       %
%==================================================================================================%

LIGHT_SPEED = 3e8;

%==================================================================================================%
% Parameters:                                                                                      %
%==================================================================================================%

% Data identifier (string)

DATA_ID = 'D_2_6-35_6-45-p3';

% --------------------------------------------------------------------------------------------------

% Project frequency [Hz]

F0 = 3e9; % FF monitor frequency

MIN_F = 2/3*F0; % project frequency range minimum
MAX_F = 4/3*F0; % ---------- || --------- maximum

% --------------------------------------------------------------------------------------------------

% Monopole radius [mm]
%  (half of monopole thickness)

MONOPOLE_RADIUS = 0.5;

% --------------------------------------------------------------------------------------------------

% Main monopole length [mm]

MAIN_MONOPOLE_LENGTH = LIGHT_SPEED/F0/4*1e3;

% --------------------------------------------------------------------------------------------------

% Ground plane padding [mm]
%   gap between ground plane edge and outer monopole ring

GND_PLANE_PADDING = 10;

% --------------------------------------------------------------------------------------------------

% Ground thickness [mm]

GND_THICKNESS = 0.1;

% --------------------------------------------------------------------------------------------------

% Gap between ground plane and monopoles [mm]

GAP = 0.1;

% --------------------------------------------------------------------------------------------------

% Number of cells per wavelength

CNUM_NEAR = 25; % near to model
CNUM_FAR = 25; % far from model

% --------------------------------------------------------------------------------------------------

% Monopole rings

% cell array of struct(L, R, A, N)
%  L ... monopole length [mm]
%  R ... ring radius [mm]
%  A ... ring rotation
%  N ... number of monopoles in ring

ml = LIGHT_SPEED/F0/4*1e3;
RINGS = {...
    struct('L', ml, 'R', 35, 'A', 0, 'N', 6); ...
    struct('L', ml, 'R', 45, 'A', pi/6, 'N', 6); ...
};

%==================================================================================================%
% Main program:                                                                                    %
%==================================================================================================%

dataDir = fullfile(cd, 'Data');
projectDir = fullfile(dataDir, DATA_ID);
cstDir = fullfile(projectDir, 'CST');
matlabDir = fullfile(projectDir, 'Matlab');

currPortProjName = 'I-Ports';
sParamProjName = 'S-Ports';

currPortProjFileName = [currPortProjName '.cst'];
sParamProjFileName = [sParamProjName '.cst'];

currPortProjPath = fullfile(cstDir, currPortProjFileName);
sParamProjPath = fullfile(cstDir, sParamProjFileName);

currPortProjDir = fullfile(cstDir, currPortProjName);
sParamProjDir = fullfile(cstDir, sParamProjName);

% --------------------------------------------------------------------------------------------------
% Prepare required directories

mkDirIfMissing(dataDir);
mkEmptyDir(projectDir);
mkdir(cstDir);
mkdir(matlabDir);

%% -------------------------------------------------------------------------------------------------
% Generate projects:

CST = CST_MicrowaveStudio(cstDir, currPortProjFileName);
setupProject(CST, 'Current');
CST.save();
CST.closeProject();

CST = CST_MicrowaveStudio(cstDir, sParamProjFileName);
setupProject(CST, 'SParam');
CST.save();
CST.closeProject();

%% -------------------------------------------------------------------------------------------------
% Solve projects:

CST = TCSTInterface();
CST.OpenProject(fullfile(cstDir, currPortProjFileName));
CST.Solve();
CST.CloseProject([], true);

CST = TCSTInterface();
CST.OpenProject(fullfile(cstDir, sParamProjFileName));
CST.Solve();
CST.CloseProject([], true);

%% -------------------------------------------------------------------------------------------------
% Generate outputs:

CST = TCSTInterface();
CST.OpenProject(fullfile(cstDir, sParamProjFileName));
[S, Freq, Zref, InfoS] = CST.GetSParams(0, [], [], [], []);
CST.CloseProject([], true);

CST = TCSTInterface();
CST.OpenProject(fullfile(cstDir, currPortProjFileName));
[Field, InfoF] = CST.ExportFarField();
CST.CloseProject([], true);

Z = convertScatToImp(S, Zref);
numPorts = size(Z, 1);

% pick index of frequency of farfield plot
f0idx = findClosestIndex(Field.Freq, Freq);
f0 = Freq(f0idx);
Z_f0 = Z(:,:,f0idx);

Theta = Field.THETA(:,1);
Phi = Field.PHI(1,:);

save(fullfile(matlabDir, 'Z.mat'), 'Z_f0');
save(fullfile(matlabDir, 'coords.mat'), 'Theta', 'Phi');

for i=1:numPorts
    E_comp = getEComponents(Field, i, 1);
    save(fullfile(matlabDir, ['ff' num2str(i) '.mat']), '-mat', 'E_comp');
end

%% ================================================================================================%
% Subroutines:                                                                                     %
%==================================================================================================%

function setupProject(CST, portType)
    global F0 MIN_F MAX_F MAIN_MONOPOLE_LENGTH MONOPOLE_RADIUS RINGS ...
        GND_PLANE_PADDING GND_THICKNESS GAP CNUM_NEAR CNUM_FAR;
    
    CST.setFreq(MIN_F*1e-9, MAX_F*1e-9);
    CST.setBoundaryCondition(...
        'xmin','open add space','xmax','open add space',...
        'ymin','open add space','ymax','open add space',...
        'zmin','electric','zmax','open add space')
    CST.configureCells(CNUM_NEAR, CNUM_FAR);
    
    gen = MonopoleArrayGenerator;
    gen.setInterface(CST);
    gen.setMainMonopoleLength(MAIN_MONOPOLE_LENGTH);
    gen.setMonopoleRadius(MONOPOLE_RADIUS);
    gen.setGroundPadding(GND_PLANE_PADDING);
    gen.setGroundThickness(GND_THICKNESS);
    gen.setGap(GAP);
    for i=1:length(RINGS)
        ring = RINGS{i};
        gen.addRing([ring.R ring.A], ring.L, zeros(ring.N, 3));
    end
    gen.setPortType(portType);
    gen.generateWithPorts();
    
    CST.addFieldMonitor('farfield', F0*1e-9);
end

function rmFileIfExists(path)
    if isfile(path)
        delete(path);
    end
end

function rmDirIfExists(path)
    if exist(path, 'dir')
        rmdir(path, 's');
    end
end

function mkDirIfMissing(path)
    if ~exist(path, 'dir')
        mkdir(path);
    end
end

function mkEmptyDir(path)
    rmDirIfExists(path);
    mkdir(path);
end
