close all;
clear;
clc;
rng default;

addpath('../lib');
loadLibs();

global THETA_RANGE PHI_RANGE;

%==================================================================================================%
% Parameters:                                                                                      %
%==================================================================================================%

% Sherical coord ranges for reference farfield clipping
%   examples: 
%     [1/2*pi 3/2*pi] ... range <1/2*pi; 3/2*pi>
%     [3/2*pi 1/2*pi] ... ranges <0; 1/2*pi> and <3/2*pi; 2*pi>

THETA_RANGE = [0 pi/2];
PHI_RANGE = [0 2*pi];

% --------------------------------------------------------------------------------------------------

% Data identifier (string)

% DATA_ID = 'A_2_6-25_6-25-p3';
% DATA_ID = 'B_2_6-22_6-28-p3';
DATA_ID = 'C-8_D-2_DD-2';

% --------------------------------------------------------------------------------------------------

% Excitation current range limit for optimizer

I_LIM = [1 100]*1e-3;

% --------------------------------------------------------------------------------------------------

% Reactance range limit for optimizer

Xamp = 1.5e3;
X_LIM = [-Xamp Xamp];

% --------------------------------------------------------------------------------------------------

% Reference farfield formula

phi_G = 0;
q_phi = 3;
REF_FF_FORMULA = @(theta, phi) ...
    abs(cos((phi-phi_G)/2))^q_phi*(cos(pi/2*cos(theta))/sin(theta));

% REF_FF_FORMULA = @(theta, phi) 100*(cos(pi/2*cos(theta))/sin(theta));

% --------------------------------------------------------------------------------------------------

% Reference farfield rotation
%   (affects THETA_RANGE and PHI_RANGE behavior)

REF_FF_ROT = [0 0];

%==================================================================================================%
% Main program:                                                                                    %
%==================================================================================================%

inputPath = fullfile(cd, 'Data', DATA_ID, 'Matlab');

coords = load(fullfile(inputPath, 'coords.mat'));
Zmat = load(fullfile(inputPath, 'Z.mat')).Z_f0;
numPorts = size(Zmat, 1);
portFF = loadPortFarfields(inputPath, numPorts);

ffRef = getReferenceFF(coords, REF_FF_ROT, REF_FF_FORMULA);

figure;
%TODO: create class dedicated for generating ref FF
plotFarField(ffRef, coords, 40.0);
title('Reference FF');

ffopt = ReactanceOptimizer;
ffopt.setPortFarfields(portFF);
ffopt.setCoordinates(coords);
ffopt.setImpedanceMatrix(Zmat);
ffopt.setExcitationCurrentBounds(I_LIM);
ffopt.setReactanceBounds(X_LIM);
ffopt.setReferenceFF(ffRef);
ffopt.setOptimizationCriterion('sum');

tic;
ffopt.run();
toc;
optimizerOut = ffopt.getOptimizerOutput()
optimizerMinCost = ffopt.getMinimumCost()

figure;
ff = ffopt.getResultFF();
plotFarField(ff, coords, 40.0);
title('Optimized FF');

resultExcitCurr = ffopt.getResultExcitationCurrent();
resultReactances = ffopt.getResultReactances();

%% ================================================================================================%
% Subroutines:                                                                                     %
%==================================================================================================%

function portFF = loadPortFarfields(directory, numPorts)
    portFF = cell(numPorts, 1);
    for i=1:numPorts
        ffPath = fullfile(directory, ['ff' num2str(i) '.mat']);
        portFF{i} = load(ffPath).E_comp(:,:,[2 3]);
    end
end

function plotFarField(ff, coords, dynRange)
    set(gca, 'FontSize', 12);
    ff = sqrt(sum(abs(ff).^2, 3));
    ff = getFieldIndB(ff, dynRange);
    plotRadiationPatternZUp(ff, coords.Theta, coords.Phi);
    colormap Turbo;
    title(colorbar, 'dB(V/m)');
end

function f_dB = getFieldIndB(f, dynRange)
    f_dB = 20*log10(f);
    threshold = max(max(f_dB))-dynRange;
    f_dB(f_dB < threshold) = threshold;
end

function ffRef = getReferenceFF(coords, rot, formula)
    ffRef = zeros(length(coords.Theta), length(coords.Phi), 2);
    ffRef(:,:,1) = getReferenceEComp(coords, rot, formula); % E_theta
    % E_phi = 0
end

function ERef = getReferenceEComp(coords, rot, formula)
    global THETA_RANGE PHI_RANGE;
    ERef = zeros(length(coords.Theta), length(coords.Phi));
    for i=1:size(ERef,1)
        for j=1:size(ERef,2)
            inTheta = clampAngle(coords.Theta(i));
            inPhi = clampAngle(coords.Phi(j)+rot(2));
            ffTheta = inTheta;
            ffPhi = inPhi;
%             [ffTheta, ffPhi] = rotSphericalX(inTheta, inPhi, rot(1));
            ffTheta = clampAngle(ffTheta);
            if isAngleInLimit(inTheta, THETA_RANGE) && isAngleInLimit(inPhi, PHI_RANGE)
                ERef(i, j) = formula(ffTheta, ffPhi);
            else
                ERef(i, j) = 0;
            end
        end
    end
    ERef(isinf(ERef)|isnan(ERef)) = 0;
end

function angle = clampAngle(angle)
    while angle < 0
        angle = angle + 2*pi;
    end
    while angle >= 2*pi
        angle = angle - 2*pi;
    end
end

function [newTheta, newPhi] = rotSphericalX(theta, phi, alpha)
    newTheta = acos(sin(theta)*sin(phi)*sin(alpha)+cos(theta)*cos(alpha));
    newPhi = atan(tan(phi)*cos(alpha)-cot(theta)*sin(alpha)*sec(phi));
end

function amp = getOptimumAmplitude(portFF)
    ff = 0;
    for i=1:length(portFF)
        ff = ff + sqrt(sum(abs(portFF{i}.E_comp).^2, 3));
    end
    amp = 2*max(max(ff))/length(portFF);
end

function res = isAngleInLimit(angle, limit)
    angle = clampAngle(angle);
    limit(1) = clampAngle(limit(1));
    limit(2) = clampAngle(limit(2));
    if limit(1) < limit(2)
        res = angle >= limit(1) && angle <= limit(2);
    else
        res = angle >= limit(1) || angle <= limit(2);
    end
end
